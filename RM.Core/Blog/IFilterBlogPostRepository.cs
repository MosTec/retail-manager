﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.Core.Blog
{
    public interface IFilterBlogPostRepository
    {
        int AddBlogPost(IBlogPost blogPost);

        bool UpdateBlogPost(IBlogPost blogPost);

        bool DeleteBlogPost(IBlogPost blogPost);

        bool DeleteBlogPostById(int id);

        IEnumerable<IBlogPost> GetAllPost();

        IBlogPost GetBlogPostById(int id);

        IEnumerable<IBlogPost> GetBlogPosts(int skip, int take);

        IEnumerable<IBlogPost> GetLast6Posts();
    }
}
