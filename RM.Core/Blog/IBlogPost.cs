﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.Core.Blog
{
    public interface IBlogPost
    {
        int Id { get; set; }

        string Title { get; set; }

        string Thumbnail { get; set; }

        string ShortDescription { get; set; }

        string Content { get; set; }

        DateTime CreateDate { get; set; }
    }
}