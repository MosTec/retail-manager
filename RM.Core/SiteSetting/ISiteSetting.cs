﻿namespace RM.Core.SiteSetting
{
    public interface ISiteSetting
    {
        int Id { get; set; }

        SiteSettingType SettingType { get; set; }

        string SettingValue { get; set; }
    }


    public enum SiteSettingType
    {
        FirstPageSliderImagePath = 1
    }
}