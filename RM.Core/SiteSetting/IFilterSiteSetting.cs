﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.Core.SiteSetting
{
    public interface IFilterSiteSettingRepository
    {
        IEnumerable<ISiteSetting> GetAllSettings();

        IEnumerable<ISiteSetting> GetSettingsByType(SiteSettingType type);

        ISiteSetting GetSettingById(int id);

        bool DeleteSettingById(int id);

        int AddSetting(ISiteSetting setting);

        int AddSettingByType(SiteSettingType type, string value);

        bool UpdateSetting(ISiteSetting setting);

        bool UpdateSettingById(int id, string value);
    }
}