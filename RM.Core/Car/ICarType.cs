﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.Core.Car
{
    public interface ICarType
    {
        int Id { get; set; }

        [Display(Name = "نوع خودرو")]
        [StringLength(50, ErrorMessage = "حداکثر 50 کاراکتر")]
        [Required(ErrorMessage = "خالی نمی تواند باشد")]
        string Type { get; set; }
    }
}