﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.Core.Car
{
    public interface ICarTypeRepository
    {
        int AddCarType(ICarType carType);

        IEnumerable<ICarType> GetAllCarTypes();

        ICarType GetCarType(int id);

        bool UpdateCarType(ICarType carType);

        bool DeleteCarType(ICarType carType);

        bool DeleteCarType(int id);
    }
}
