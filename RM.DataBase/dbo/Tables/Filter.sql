﻿CREATE TABLE [dbo].[Filter]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FilterCategoryId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Thumbnail] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_Filter_ToFilterCategory] FOREIGN KEY ([FilterCategoryId]) REFERENCES [FilterCategory]([Id])
)
