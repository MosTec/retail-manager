﻿CREATE TABLE [dbo].[FilterCarFilter]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CarId] INT NOT NULL, 
    [FilterId] INT NOT NULL, 
    CONSTRAINT [FK_FilterCarFilter_ToFilter] FOREIGN KEY ([FilterId]) REFERENCES [Filter]([Id]), 
    CONSTRAINT [FK_FilterCarFilter_ToCar] FOREIGN KEY ([CarId]) REFERENCES [FilterCar]([Id])
)
