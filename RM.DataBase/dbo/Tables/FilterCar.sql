﻿CREATE TABLE [dbo].[FilterCar]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CarMakeId] INT NOT NULL, 
    [CarName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_FilterCar_ToFilterCarMake] FOREIGN KEY ([CarMakeId]) REFERENCES [FilterCarMake]([Id])
)
