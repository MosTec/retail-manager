﻿CREATE TABLE [dbo].[FilterCarMake]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CarTypeId] INT NOT NULL, 
    [MakeName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_FilterCarMake_ToCarType] FOREIGN KEY ([CarTypeId]) REFERENCES [FilterCarType]([Id])
)
