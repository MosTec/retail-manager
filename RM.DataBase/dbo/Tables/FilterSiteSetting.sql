﻿CREATE TABLE [dbo].[FilterSiteSetting]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [SettingType] INT NOT NULL, 
    [SettingValue] NVARCHAR(MAX) NOT NULL
)
