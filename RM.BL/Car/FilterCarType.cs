﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.Car;
using RM.DAL.Car;

namespace RM.BL.Car
{
    public class FilterCarType
    {
        private readonly ICarTypeRepository _carTypeRepository;

        public FilterCarType(ICarTypeRepository carTypeRepository)
        {
            _carTypeRepository = carTypeRepository;
        }
        public IEnumerable<CarType> GetAllCarTypes()
        {
            return _carTypeRepository.GetAllCarTypes() as IEnumerable<CarType>;
        }

        public void AddCarType(CarType carType)
        {
            _carTypeRepository.AddCarType(carType);
        }

        public CarType GetCarType(int id)
        {
            return _carTypeRepository.GetCarType(id) as CarType;
        }

        public void DeleteCarType(int id)
        {
            _carTypeRepository.DeleteCarType(id);
        }
    }
}
