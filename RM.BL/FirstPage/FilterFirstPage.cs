﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.Blog;
using RM.Core.SiteSetting;
using RM.DAL.Blog;

namespace RM.BL.FirstPage
{
    public class FilterFirstPage
    {
        private readonly IFilterSiteSettingRepository _filterSiteSetting;
        private readonly IFilterBlogPostRepository _blogPostRepository;

        public FilterFirstPage(IFilterSiteSettingRepository filterSiteSetting, IFilterBlogPostRepository blogPostRepository)
        {
            _filterSiteSetting = filterSiteSetting;
            _blogPostRepository = blogPostRepository;
        }

        public IEnumerable<string> GetFirstPageSlider()
        {
            return _filterSiteSetting.GetSettingsByType(SiteSettingType.FirstPageSliderImagePath)
                .Select(p => p.SettingValue).ToList();
        }

        public IEnumerable<BlogPost> Get6LastPost()
        {
            return _blogPostRepository.GetLast6Posts() as IEnumerable<BlogPost>;
        }
    }
}
