﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.Blog;

namespace RM.BL.FirstPage
{
    public class FilterBlogPost : IBlogPost
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
      
            
   
    }
}