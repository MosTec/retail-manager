﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RM.BL.Helper
{
    public static class PersianDate
    {
        public static string ToPersianDateYYMMDD(this DateTime value)
        {
            var pc = new PersianCalendar();
            return $"{pc.GetYear(value)}/{pc.GetMonth(value):00}/{pc.GetDayOfMonth(value):00}";
        }
        public static string ToPersianDateMMDD(this DateTime value)
        {
            var pc = new PersianCalendar();
            return $"{pc.GetDayOfMonth(value):00} {PersianMonthName(pc.GetMonth(value).ToString("00"))}";
        }

        private static string PersianMonthName(string monthDigit)
        {
            switch (monthDigit)
            {
                case "01":
                    return "فروردین";
                case "02":
                    return "اردیبهشت";
                case "03":
                    return "خرداد";
                case "04":
                    return "تیر";
                case "05":
                    return "مرداد";
                case "06":
                    return "شهریور";
                case "07":
                    return "مهر";
                case "08":
                    return "آبان";
                case "09":
                    return "آذر";
                case "10":
                    return "دی";
                case "11":
                    return "بهمن";
                case "12":
                default:
                    return "اسفند";
            }
        }
    }
}