﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.SiteSetting;

namespace RM.DAL.SiteSetting
{
    public class SiteSetting : ISiteSetting
    {
        public int Id { get; set; }
        public SiteSettingType SettingType { get; set; }
        public string SettingValue { get; set; }
    }
}
