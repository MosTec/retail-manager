﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using RM.Core.SiteSetting;

namespace RM.DAL.SiteSetting
{
    public class FilterSiteSettingRepository : IFilterSiteSettingRepository
    {
        public IEnumerable<ISiteSetting> GetAllSettings()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var sql = $"SELECT * FROM [dbo].[FilterSiteSetting];";

                return connection.Query<SiteSetting>(sql).ToList();
            }
        }

        public IEnumerable<ISiteSetting> GetSettingsByType(SiteSettingType type)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Type", type);

                var sql = $"SELECT * FROM [dbo].[FilterSiteSetting] WHERE [SettingType] = @type;";

                return connection.Query<SiteSetting>(sql, p).ToList();
            }
        }

        public ISiteSetting GetSettingById(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"SELECT * FROM [dbo].[FilterSiteSetting] WHERE [Id] = @Id;";

                return connection.Query<SiteSetting>(sql, p).Single();
            }
        }

        public bool DeleteSettingById(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"DELETE FROM [dbo].[FilterSiteSetting] WHERE [Id] = @Id;";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public int AddSetting(ISiteSetting setting)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", 0, DbType.Int32, ParameterDirection.Output);
                p.Add("@SettingType", setting.SettingType);
                p.Add("@SettingValue", setting.SettingValue);

                var sql = $"INSERT INTO [dbo].[FilterSiteSetting] ( [SettingType],[SettingValue] )" +
                                $" VALUES (@SettingType, @SettingValue);" +
                                $"SELECT @Id = @@IDENTITY;";

                connection.Execute(sql, p);

                return p.Get<int>("@Id");
            }
        }

        public int AddSettingByType(SiteSettingType type, string value)
        {
            var setting = new SiteSetting() {SettingType = type, SettingValue = value};
            return AddSetting(setting);
        }

        public bool UpdateSetting(ISiteSetting setting)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", setting.Id);
                p.Add("@SettingValue", setting.SettingValue);

                var sql = $"UPDATE [dbo].[FilterSiteSetting] SET [SettingValue] = @SettingValue WHERE [Id] = @Id;";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool UpdateSettingById(int id, string value)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);
                p.Add("@SettingValue", value);

                var sql = $"UPDATE [dbo].[FilterSiteSetting] SET [SettingValue] = @SettingValue WHERE [Id] = @Id;";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}