﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using RM.Core.Blog;

namespace RM.DAL.Blog
{
    public class FilterBlogPostRepository : IFilterBlogPostRepository
    {
        public int AddBlogPost(IBlogPost blogPost)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", 0, DbType.Int32, ParameterDirection.Output);
                p.Add("@Title", blogPost.Title);
                p.Add("@Thumbnail", blogPost.Thumbnail);
                p.Add("@ShortDescription", blogPost.ShortDescription);
                p.Add("@Content", blogPost.Content);

                var sql = $"INSERT INTO [dbo].[FilterSiteBlog] ( [Title],[Thumbnail],[ShortDescription],[Content]) " +
                          $"VALUES (@Title, @Thumbnail, @ShortDescription, @Content);" +
                          $"SELECT @Id = @@IDENTITY;";

                connection.Execute(sql, p);

                return p.Get<int>("@Id");
            }
        }

        public bool UpdateBlogPost(IBlogPost blogPost)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", blogPost.Id);
                p.Add("@Title", blogPost.Title);
                p.Add("@Thumbnail", blogPost.Thumbnail);
                p.Add("@ShortDescription", blogPost.ShortDescription);
                p.Add("@Content", blogPost.Content);

                var sql =
                    $"UPDATE [dbo].[FilterSiteBlog] SET [Title] = @Title, [Thumbnail] = @Thumbnail, [ShortDescription] = @ShortDescription, [Content] = @Content WHERE [Id] = @Id ";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool DeleteBlogPost(IBlogPost blogPost)
        {
            return DeleteBlogPostById(blogPost.Id);
        }

        public bool DeleteBlogPostById(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"DELETE FROM [dbo].[FilterSiteBlog] WHERE [Id] = @Id;";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public IEnumerable<IBlogPost> GetAllPost()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var sql = $"SELECT * FROM [dbo].[FilterSiteBlog];";

                return connection.Query<BlogPost>(sql).ToList();
            }
        }

        public IBlogPost GetBlogPostById(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"SELECT * FROM [dbo].[FilterSiteBlog] WHERE [Id]=@Id;";

                return connection.Query<BlogPost>(sql, p).Single();
            }
        }

        public IEnumerable<IBlogPost> GetBlogPosts(int skip, int take)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Skip", skip);
                p.Add("@Take", take);

                var sql =
                    $"SELECT * FROM [dbo].[FilterSiteBlog] ORDER BY [CreateDate] OFFSET (@Skip) ROWS FETCH NEXT (@Take) ROWS ONLY;";

                return connection.Query<BlogPost>(sql, p).ToList();
            }
        }

        public IEnumerable<IBlogPost> GetLast6Posts()
        {
            return GetBlogPosts(0, 6);
        }
    }
}