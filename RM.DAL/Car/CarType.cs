﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.Car;

namespace RM.DAL.Car
{   
    [MetadataType(typeof(ICarType))]
    public class CarType : ICarType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
