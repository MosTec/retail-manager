﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RM.Core.Car;
using Dapper;

namespace RM.DAL.Car
{
    public class CarTypeRepository : ICarTypeRepository
    {
        public int AddCarType(ICarType carType)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", 0, DbType.Int32, ParameterDirection.Output);
                p.Add("@CarType", carType.Type);

                var sql = $"INSERT INTO [dbo].[FilterCarType] ( [Type] )" +
                          $" VALUES (@CarType);" +
                          $"SELECT @Id = @@IDENTITY;";

                connection.Execute(sql, p);

                return p.Get<int>("@Id");
            }
        }

        public IEnumerable<ICarType> GetAllCarTypes()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var sql = $"SELECT * FROM [dbo].[FilterCarType];";

                return connection.Query<CarType>(sql).ToList();
            }
        }

        public ICarType GetCarType(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"SELECT * FROM [dbo].[FilterCarType] WHERE [Id]=@Id;";

                return connection.QueryFirstOrDefault<CarType>(sql, p);
            }
        }

        public bool UpdateCarType(ICarType carType)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", carType.Id);
                p.Add("@CarType", carType.Type);

                var sql =
                    $"UPDATE [dbo].[FilterCarType] SET [Type] = @CarType WHERE [Id] = @Id ";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool DeleteCarType(ICarType carType)
        {
            return DeleteCarType(carType.Id);
        }

        public bool DeleteCarType(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.Helper.CnnVal()))
            {
                var p = new DynamicParameters();

                p.Add("@Id", id);

                var sql = $"DELETE FROM [dbo].[FilterCarType] WHERE [Id] = @Id;";

                try
                {
                    connection.Execute(sql, p);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
