﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RM.MVCWebUI.Areas.FilterAdmin.Controllers
{
    public class HomeController : Controller
    {
        // GET: FilterAdmin/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}