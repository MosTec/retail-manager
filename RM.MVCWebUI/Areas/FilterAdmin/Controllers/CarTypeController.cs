﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RM.BL.Car;
using RM.Core.Car;
using RM.DAL.Car;

namespace RM.MVCWebUI.Areas.FilterAdmin.Controllers
{
    public class CarTypeController : Controller
    {
        private readonly FilterCarType _carType;

        public CarTypeController(FilterCarType carType)
        {
            _carType = carType;
        }
        // GET: FilterAdmin/CarType
        public ActionResult Index()
        {
            var result = _carType.GetAllCarTypes();
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CarType carType)
        {
            if (ModelState.IsValid)
            {
                _carType.AddCarType(carType);
                return RedirectToAction("Index");
            }

            return View(carType);
        }

        public ActionResult Delete(int? id)
        {
            if (id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var result = _carType.GetCarType(id.Value);

            if (result==null)
            {
                return HttpNotFound();
            }

            return View(result);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _carType.DeleteCarType(id);
            return RedirectToAction("Index");
        }
    }
}