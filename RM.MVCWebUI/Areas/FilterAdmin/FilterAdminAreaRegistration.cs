﻿using System.Web.Mvc;

namespace RM.MVCWebUI.Areas.FilterAdmin
{
    public class FilterAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FilterAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FilterAdmin_default",
                "FilterAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new []{ "RM.MVCWebUI.Areas.FilterAdmin.Controllers" }
            );
        }
    }
}