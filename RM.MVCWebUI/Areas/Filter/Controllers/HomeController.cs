﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RM.BL.FirstPage;
using RM.MVCWebUI.ViewModel;

namespace RM.MVCWebUI.Areas.Filter.Controllers
{
    public class HomeController : Controller
    {
        private readonly FilterFirstPage _firstPage;

        public HomeController(FilterFirstPage firstPage)
        {
            _firstPage = firstPage;
        }

        // GET: Home
        public ActionResult Index()
        {

            var viewModel = new FilterFirstPageViewModel
            {
                FirstPageImageSlider = _firstPage.GetFirstPageSlider().ToList(),
                TopBlogPost = _firstPage.Get6LastPost().ToList()

            };

            return View(viewModel);
        }

        // GET: Contact US
        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(string contactUsViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(contactUsViewModel);
            }

            return View();
        }


        // GET: Company-History
        public ActionResult CompanyHistory()
        {
            return View();
        }

        // Get: ISO-Certificate
        public ActionResult ISOCertificate()
        {
            return View();
        }

        // Get: Company-capabilities
        public ActionResult CompanyCapabilities()
        {
            return View();
        }

        // Get: Company-Goals

        public ActionResult CompanyGoals()
        {
            return View();
        }


        [HttpPost]
        public ActionResult JoinNewsLetter(string joinNewsLetter)
        {
            if (ModelState.IsValid)
            {
            }

            return View("Index");
        }
    }
}