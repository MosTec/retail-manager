﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RM.Core.SiteSetting;
using RM.DAL.Blog;

namespace RM.MVCWebUI.ViewModel
{
    public class FilterFirstPageViewModel
    {
        public List<string> FirstPageImageSlider { get; set; }
        public List<BlogPost> TopBlogPost { get; set; }
    }
}