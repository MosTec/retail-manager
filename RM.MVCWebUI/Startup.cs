﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RM.MVCWebUI.Startup))]
namespace RM.MVCWebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
