using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using RM.BL.Car;
using RM.BL.FirstPage;
using RM.Core.Blog;
using RM.Core.Car;
using RM.Core.SiteSetting;
using RM.DAL.Blog;
using RM.DAL.Car;
using RM.DAL.SiteSetting;

namespace RM.MVCWebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Register application dependencies.
            //builder.RegisterType<MyClass>().As<IMyClass>();

            builder.RegisterType<FilterSiteSettingRepository>().As<IFilterSiteSettingRepository>();
            builder.RegisterType<FilterBlogPostRepository>().As<IFilterBlogPostRepository>();
            builder.RegisterType<CarTypeRepository>().As<ICarTypeRepository>();

            builder.RegisterType<FilterFirstPage>().AsSelf();
            builder.RegisterType<FilterCarType>().AsSelf();
      

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
